<div class="jumbotron">
    <div class="container">
        <h1>King Kong Mahjong</h1>
        <h2>Under Construction</h2>
        <p>This page is currently under construction. Come back soon and it may well be complete and whole again.</p>
        <br>
        <p><a class="btn btn-primary btn-lg" href="../../index.php" role="button">Home »</a></p>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-11">
            <p>This page does not yet exist.</p>
            <p>But keep an eye out as this project is under constant development.</p>
            <p>So this page should be active soon.<br /></p>
        </div>

        <div class="col-md-1 footer-button-height">
            <a class="btn btn-default footer-home-button" href="index.php">Return to Main Menu</a>
        </div>
    </div>

</div>