<?php
/* 
 * Copyright (C) 2017 Digital Alchemy
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
?>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!--jQuery BEFORE Bootstrap-->
<!--    <script type="text/javascript" src="js/jquery-1.8.0.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.js"
            integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA="
            crossorigin="anonymous">
    </script>-->
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    
    <script src="js/bootstrap.min.js"></script>
    
    <!--<link href="css/bootstrap.min.css" rel="stylesheet">-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    
    <link href="<?= CSS; ?>main.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    
    <title><?= ( isset($title) ? $title . ' - King Kong Mahjong' : 'King Kong Mahjong' ); ?></title>
</head>
<body>    
    <?php $login_register       = ( isset($_SESSION['user']['user_id']) ? 'Logout' : 'Login/Register' ); ?>
    <?php $login_register_url   = ( isset($_SESSION['user']['user_id']) ? URL . 'index.php/login/logout' : URL . 'index.php/login/login' ); ?>
    
    <nav class="navbar navbar-inverse navbar-static-top">
	<div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">King Kong Mahjong</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<ul class="nav navbar-nav navbar-right">
                    <li><a href="<?= $login_register_url; ?>"><?= $login_register; ?></a></li>
                    <li><a href="#">About</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Actions<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Games</a></li>
                            <li><a href="#">Players</a></li>
                            <li><a href="#">Options</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Contact</a></li>
		</ul>
            </div>
	</div>
    </nav>
    
    <div class="message <?= ( isset($this->session->message_status) ? $this->session->message_status : '' ); ?>">
        <?= ( isset($this->session->message) ? $this->session->message : '' ); ?>
        <?php
            $this->session->unset_userdata('message');
            $this->session->unset_userdata('message_status');
        ?>
    </div>
