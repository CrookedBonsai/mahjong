<div class="container">
    
    <div class="row">
        <h1>Login / Register</h1>
        <div class="col-md-3"></div>
        <div class="col-md-3">
            <h2>Log In</h2>
            <?= form_open('login/login', $view_data['form']['login']['form_params']); ?>
            <div class="row">
                <?= form_label($view_data['form']['login']['email']['label'], 'email'); ?>
                <?= form_input($view_data['form']['login']['email']); ?>
                <div class="form_error"><?= form_error('email'); ?></div>
            </div>
            <div class="row">
                <?= form_label($view_data['form']['login']['password']['label'], 'password'); ?>
                <?= form_password($view_data['form']['login']['password']); ?>
                <div class="form_error"><?= form_error('password'); ?></div>
            </div>
            <div class="row">
                <?= form_submit($view_data['form']['login']['submit']); ?>
            </div>
            <?= form_close(); ?>
        </div>
        <div class="col-md-3">
            <h2>Register</h2>
            <?= form_open('login/login', $view_data['form']['register']['form_params']); ?>
            <div class="row">
                <?= form_label($view_data['form']['register']['first_name']['label'], 'first_name'); ?>
                <?= form_input($view_data['form']['register']['first_name']); ?>
                <div class="form_error"><?= form_error('first_name'); ?></div>
            </div>
            <div class="row">
                <?= form_label($view_data['form']['register']['surname']['label'], 'surname'); ?>
                <?= form_input($view_data['form']['register']['surname']); ?>
                <div class="form_error"><?= form_error('surname'); ?></div>
            </div>
            <div class="row">
                <?= form_label($view_data['form']['register']['nickname']['label'], 'nickname'); ?>
                <?= form_input($view_data['form']['register']['nickname']); ?>
                <div class="form_error"><?= form_error('nickname'); ?></div>
            </div>
            <div class="row">
                <?= form_label($view_data['form']['register']['reg_email']['label'], 'reg_email'); ?>
                <?= form_input($view_data['form']['register']['reg_email']); ?>
                <div class="form_error"><?= form_error('reg_email'); ?></div>
            </div>
            <div class="row">
                <?= form_label($view_data['form']['register']['reg_password']['label'], 'reg_password'); ?>
                <?= form_password($view_data['form']['register']['reg_password']); ?>
                <div class="form_error"><?= form_error('reg_password'); ?></div>
            </div>
            <div class="row">
                <?= form_label($view_data['form']['register']['reg_confirm_password']['label'], 'reg_confirm_password'); ?>
                <?= form_password($view_data['form']['register']['reg_confirm_password']); ?>
                <div class="form_error"><?= form_error('reg_confirm_password'); ?></div>
            </div>
            <div class="row">
                <?= form_submit($view_data['form']['register']['submit']); ?>
            </div>
            <?= form_close(); ?>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>