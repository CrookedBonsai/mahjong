  
<div class="jumbotron">
    <div class="container">
        <h1>Welcome to King Kong Mahjong.</h1>
        <p>Create a game, set up your players and start playing.</p>
        <p>Enter your scores and we will take care of the tricky calculations and keep track of who is winning, leaving you free to focus on getting your next "Kong"!</p>
        <br>
        <p><a class="btn btn-primary btn-lg" href="#" role="button">Start a New Game »</a></p>
    </div>
</div>

<div class="container">      
<!--        <div id="body">
        <p>This is a temporary home page</p>
        <p>This page is currently under construction and will be available soon</p>
        <p>From here you will be able to create Users, games and manage scores.</p>
    </div>-->
    <section class="call-to-action">
        <div class="row">
            <div class="col-md-4">
                <a href="index.php/not_yet/">
                    <div class="menu-option">

                        <!--<h3><span class="glyphicon glyphicons-playing-dices" aria-hidden="true"></span> Games</h3>-->
                        <span class="glyphicon glyphicon-play-circle glyphicon-large" aria-hidden="true"></span>
                        <h3>Games</h3>
                    </div>
                </a>
                <p>Create, update and continue your games here. <br />For all game related actions, <br />this is the place to be.</p>
            </div>

            <div class="col-md-4">
                <a href="index.php/not_yet/">
                    <div class="menu-option">
                        <!--<h3><span class="glyphicon glyphicons-group" aria-hidden="true"></span> Players</h3>-->
                        <span class="glyphicon glyphicon-user glyphicon-large" aria-hidden="true"></span>
                        <h3>Players</h3>
                    </div>
                </a>
                <p>Create Players, edit players, change which <br />players are playing which game.</p>
            </div>
            <div class="col-md-4">
                <a href="index.php/not_yet/">
                    <div class="menu-option">
                        <!--<h3><span class="glyphicon glyphicons-settings" aria-hidden="true"></span> Players</h3> Options</h3>-->
                        <span class="glyphicon glyphicon-wrench glyphicon-large" aria-hidden="true"></span>
                        <h3>Options</h3>
                    </div>
                </a>
                <p>All the options and settings you can <br />shake a Prevailing Wind at.</p>
            </div>
        </div>
    </section>
<!--</div>-->

