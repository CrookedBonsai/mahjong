<?php

/* 
 * Copyright (C) 2017 Digital Alchemy
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class Login_model extends CI_Model
{    
    public function __construct() 
    {
        parent::__construct();
        
        $this->load->library('session');
        $this->load->config('mahjong_config');
    }
    
    
    /**
     * Create Login Form
     * 
     * Generate the form and fields for the Login Form
     */
    public function createLoginForm($post_data)
    {
        $fields = array(
            'form_params' => array(
                'class' => 'login-form',
                'id'    => 'login-form'
            ),
            'email'     => array(
                'name'          => 'email',
                'id'            => 'email',
                'value'         => ( isset($post_data['email']) ? $post_data['email'] : null ),
                'max_length'    => '250',
                'placeholder'   => 'Email Address',
                'label'         => 'Email Address'
            ),
            'password'  => array(
                'name'          => 'password',
                'id'            => 'password',
                'value'         => ( isset($post_data['password']) ? $post_data['password'] : null ),
                'label'         => 'Password',
                'placeholder'   => 'Password'
            ),
            'submit'    => array(
                'name'          => 'login',
                'value'         => 'Log In',
                'class'         => 'btn btn-default'
            )
        );
        return $fields;
    }
    
    
    /**
     * Create Register Form
     * 
     * Generate the form and fields for the Register Form
     */
    public function createRegisterForm($post_data)
    {
        $fields = array(
            'form_params' => array(
                'class' => 'register-form',
                'id'    => 'register-form'
            ),
            'first_name'     => array(
                'name'          => 'first_name',
                'id'            => 'first_name',
                'value'         => ( isset($post_data['first_name']) ? $post_data['first_name'] : null ),
                'max_length'    => '250',
                'placeholder'   => 'First Name',
                'label'         => 'First Name'
            ),
            'surname'     => array(
                'name'          => 'surname',
                'id'            => 'surname',
                'value'         => ( isset($post_data['surname']) ? $post_data['surname'] : null ),
                'max_length'    => '250',
                'placeholder'   => 'Surname',
                'label'         => 'Surname'
            ),
            'nickname'     => array(
                'name'          => 'nickname',
                'id'            => 'nickname',
                'value'         => ( isset($post_data['nickname']) ? $post_data['nickname'] : null ),
                'max_length'    => '250',
                'placeholder'   => 'Nickname',
                'label'         => 'Nickname'
            ),
            'reg_email'     => array(
                'name'          => 'reg_email',
                'id'            => 'email',
                'value'         => ( isset($post_data['reg_email']) ? $post_data['reg_email'] : null ),
                'max_length'    => '250',
                'placeholder'   => 'Email Address',
                'label'         => 'Email Address'
            ),
            'reg_password'  => array(
                'name'          => 'reg_password',
                'id'            => 'password',
                'value'         => ( isset($post_data['reg_password']) ? $post_data['reg_password'] : null ),
                'label'         => 'Password',
                'placeholder'   => 'Password'
            ),
            'reg_confirm_password'  => array(
                'name'          => 'reg_confirm_password',
                'id'            => 'confirm_password',
                'value'         => ( isset($post_data['reg_confirm_password']) ? $post_data['reg_confirm_password'] : null ),
                'label'         => 'Confirm Password',
                'placeholder'   => 'Confirm Password'
            ),
            'submit'    => array(
                'name'          => 'register',
                'value'         => 'Register',
                'class'         => 'btn btn-default'
            )
        );
        return $fields;
    }
    
    
    /**
     * Verify Password
     * 
     * Check the user entered password against the stored password hash
     * for the entered email address. Returns User ID is successful and 0 (false) if not
     * 
     * @param array $post_data
     * @return int
     */
    public function verify_password($post_data)
    {
        // check if password is correct for that user
        $result         = false;
        $email          = $post_data['email'];
        $password       = $post_data['password'];
        $error          = array('code' => 1111, 'message' => null);
        
        $sql = 
            "SELECT * FROM user " .
            "WHERE email = " . $this->db->escape($email) . " " .
            "AND user_status_id = " . $this->config->item('active', 'user') . " " . 
            "LIMIT 1";
        
        $query = $this->db->query($sql);
        
        if ($found_user = $query->row())
        {
            $hash = $found_user->password;
            
            $result = password_verify($password, $hash);
            
            if (!$result)
            {
                $error['message'] = 'Passwords do not match';
            }
        }
        else
        {
            log_message('debug', 'Login failed. User not found for: ' . $email);
            $error['message'] = "User does not exist";
        }
        
        if ($result == true)
        {
            // Set the session
            log_message('info', 'Login success for: ' . $email);
        }
        else
        {
            // Log the error message
            $error = ( !empty($error['message']) ? $error : $this->db->error() );
            if ($error['code'] != 0)
            {
                log_message('debug', 'Login attempt error for: ' . $email . ' -> ' . $error['code'] . ': ' . $error['message']);
            }
        }
        
        $response = array(
            'result'    => $result,
            'user_id'   => ( isset($found_user->user_id) ? $found_user->user_id : 0)
        );
        
        return $response;
    }
    
    
    /**
     * Register User
     * 
     * Create a password hash and save the user in the database 
     * as long as the email address doesnt already exist
     * 
     * @param array $post_data
     * @return array
     */
    public function register_user($post_data)
    {        
        // Generate the encrypted password hash
        $password_hash = password_hash($post_data['reg_password'], PASSWORD_DEFAULT);        
        
        foreach ($post_data as $key => &$value)
        {
            $value = $this->db->escape($value);
        }
        
        // Begin Transaction
        $this->db->trans_start(TRUE);
        
        // Check if email already exists as a user and fail if it does
        $sql1           = "SELECT user_id FROM user WHERE email = {$post_data['reg_email']}";
        $user_exists    = $this->db->query($sql1);
        
        if(empty($user_exists->num_rows()))
        {
            $sql = 
                "INSERT INTO user " . 
                "(first_name, surname, nickname, email, password) " . 
                "VALUES ({$post_data['first_name']}, {$post_data['surname']}, {$post_data['nickname']}, {$post_data['reg_email']}, '{$password_hash}') ";

            $query      = $this->db->query($sql);
            $user_id    = ( $query  ? $this->db->insert_id() : 0 );
            $error      = ( !$query ? $this->db->error()     : null );
        }
        else
        {
            $query      = false;
            $user_id    = 0;
            $error      = array('code' => 1111, 'message' => 'This user already exists.');
        }
        $this->db->trans_complete();
        
        $response = array(
            'result'    => $query,
            'user_id'   => $user_id,
            'error'     => $error
        );
        
        return $response;
    }
}
