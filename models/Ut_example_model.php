<?php

/* 
 * Test Model for testing connections to the database etc
 */
class Ut_example_model extends CI_Model
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->database();
        
        $this->example_array = array(
            "Item1" => 1,
            "Item2" => 2,
            "Item3" => 3,
            "Item4" => 4,
            "Item5" => 5
        );
    }
    
    public function test()
    {
        
        echo "\nTest Model Function Found";
        return true;
    }
    
    /**
     * Return the example array
     * @return array
     */
    public function case1()
    {
        return $this->example_array;
    }
    
    
    /**
     * Recieve an array and add a zero to each value
     * @param array $passed_array
     * @return array
     */
    public function case2(array $passed_array)
    {
        foreach ($passed_array as $key => $value)
        {
            $passed_array[$key] = $value * 11;
        }
        return $passed_array;
    }
}

