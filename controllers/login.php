<?php

/* 
 * Copyright (C) 2017 Digital Alchemy
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
class Login extends CI_Controller
{
    public function __construct() 
    {
        parent::__construct();
        
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation', 'session');
        $this->load->model('login_model');
        $this->load->config('mahjong_config');
    }
    
    
    /*
     * Display Login Screen
     * 
     * Show the login screen for entering user login credentials 
     * Or registering an account.
     * Handles the POST data and sends it off to the model to
     * be verified and processed.
     * 
     * 
     */
    public function login()
    {       
        // Capture the login/register details and determine if logging in or registering
        $post_data = ( isset($_POST) ? $_POST : null );
        
        $view_data['form']['login']     = $this->login_model->createLoginForm($post_data);
        $view_data['form']['register']  = $this->login_model->createRegisterForm($post_data);
        
        // Register User
        if (isset($post_data['register']))
        {
            if ($this->form_validation->run('login/register'))
            {
                $this->register_user($view_data);
            }
        }
        // Log In User
        else
        {
            if (!$this->form_validation->run('login/login'))
            {
                $this->load_login_view($view_data);
            }
            else
            {
                $result = $this->log_in_user($post_data);

                if ($result)
                {
                    $this->session->message         = 'You have successfully logged in';
                    $this->session->message_status  = 'success';

                    log_message('info', 'Successful login by user: ' . $post_data['email']);
                    redirect('home');
                }
                else
                {
                    // Capture, log and feedback the failed attempt
                    $error                          = ( isset($register_result['error']) ? $register_result['error'] : 'No Error Reported' );
                    $this->session->message         = 'We were unable to log you in. Please try again.';
                    $this->session->message_status  = 'error';

                    log_message('debug', 'UNSUCCESSFUL login attempt by user: ' . $post_data['email'] . ' ERROR: ' . $error);
                }
            }  
        }
        $this->load_login_view($view_data, array('data' => $view_data));
    }
    
     /**
     * Register User
     * Create a user using the supplied details
     * Return 0(false) if unsuccessful or User ID if successful
     * 
     * @param type $post_data
     * @return int
     */
    public function register_user()
    {
        // Capture the login/register details and determine if logging in or registering
        $post_data  = ( isset($_POST) ? $_POST : null );
        $result     = false;
        
        if (!$this->form_validation->run())
        {
            return $result;
        }
        else
        {
            // Rrgister the new uers details
            $register_result    = $this->login_model->register_user($post_data);
            $result             = $register_result['result'];
            
            if ($result)
            {
                $user_id                        = ( isset($register_result['user_id']) ? $register_result['user_id'] : 0 );
                $this->session->message         = 'You have successfully registered and account. Welcome to King Kong Mahjong';
                $this->session->message_status  = 'success';
                
                log_message('info', 'New user registered: ' . $user_id);
                
                // Set the session
                $_SESSION['user']['user_id']    = $user_id;
                $_SESSION['user']['email']      = $post_data['reg_email'];
                
                redirect('home');
            }
            else
            {
                // Clear any active sessions
                $this->session->unset_userdata('user');
                
                // Capture, log and feedback the failed attempt
                $error                          = ( isset($register_result['error']) ? $register_result['error'] : 'No Error Reported' );
                $this->session->message         = 'We were unable to complete your registration. Please try again or contact an administrator.';
                $this->session->message_status  = 'error';
                
                log_message('debug', "UNSUCCESSFUL registration attempt by user: {$post_data['reg_email']} - ERROR: {$error['code']} - {$error['message']}");
            }
        }   
        return $result;
    }
    
    
    private function load_login_view ($view_data = array())
    {
        $view_data['title'] = 'Login/Register';
        
        $this->load->view('layout/header', $view_data);
        $this->load->view('login', array('view_data' => $view_data));
        $this->load->view('layout/footer');
    }
        
    
    /**
     * Log In User
     * 
     * Attempt to log the user in with the supplied credentials
     * Set the Session details if successful.
     * return 0 (false) if unsuccessful or User ID if successful
     * 
     * @param array $post_data
     * @return int
     */
    private function log_in_user($post_data)
    {
        // check if password is correct for that user
        $check_login    = $this->login_model->verify_password($post_data);
        $result         = $check_login['result'];
        $user_id        = $check_login['user_id'];
        
        if ($result == true)
        {
            // Set the session
            $_SESSION['user']['user_id']   = $user_id;
            $_SESSION['user']['email']     = $post_data['email'];
        }
        else
        {
            // Clear any active sessions
            $this->session->unset_userdata('user');
        }
        return $result;
    }
   
    
    public function logout ()
    {
        $user_id    = $this->session->user['user_id'];
        $email      = $this->session->user['email'];
        
        $this->session->unset_userdata('user');
        
        $this->session->message         = 'You have been successfully logged out.';
        $this->session->message_status  = 'success';
                
        log_message('info', "User logged out: User ID -> {$user_id}");
        
        redirect('home');
    }

}

