<?php

//// Including Composer Autoload
//require (__dir__ . '/vendor/autoload.php');

/* 
 * A test Controller for testing out Unit Testing theories, syntaxes and configurations
 */
class UtExample extends CI_Controller
{   
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('Ut_example_model');    
    }
    
    /**
     * a simple test function to connect to the model and echo a value to prove connection
     */
    public function simple()
    {
        $this->Ut_example_model->test();
        $sample = 12;
        echo "\nTest Function found";
        print_r("\nSample = " . $sample);
    }
    
    
    /**
     * Simple function to return integer 1
     * @return int
     */
    public function case1()
    {
        return 1;
    }
    
    
    /**
     * Simple function to return the recieved value
     * @param type $var
     * @return type
     */
    public function case2($var)
    {
        return $var;
    }
    
    /**
     * Testing connection to model
     * @return type
     */
    public function case3()
    {
        $a = $this->Ut_example_model->test();
        echo "\nConnection to Model = ";
        echo ($a ? "True" : "False");
        return $a;
    }
}

