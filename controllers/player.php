<?php

/* 
 * Created by Andy Carr
 */

class Player extends CI_Controller
{    
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model("player_model");
    }
    
    // Player Parameters
    var $first_name = "";
    var $last_name  = "";
    var $nickname   = "";
    
    
    public function test()
    {
        echo "Player Alive";
        return "Success";
    }
    
    /**
     * Validate Input
     */
    public function validate_player_info($player_data)
    {
        // Extract the data from the player_info array
        // Validate it
        // Format it appropriately
        // Return formatted array if correct
        // or errors if not
        return $player_data;
    }
    
    
    /**
     * Create a player
     */
    public function set_player($player_data)
    {        
        // first_name
        // last_name
        // nickname
        // Save the data in the Database
    }
    
    /**
     * Get a player
     */
    public function get_player($player_id)
    {
        // Get the player data from the database by id
    }
    
    
    /**
     * Edit a player
     */
    public function edit_player($player_id, $player_data)
    {
        // Update the info from $player_data 
        // for player: $player_id
        // where it has changed
    }
    
}
