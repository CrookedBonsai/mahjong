<?php

/* 
 * Copyright (C) 2017 Digital Alchemy
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// Login Form Validation

        
$config = array(
    'login/login' => array(
        array(
            'field' => 'email', 
            'label' => 'Email Address', 
            'rules' => 'required|valid_email|trim|htmlspecialchars'
        ),
        array(
            'field' => 'password', 
            'label' => 'Password', 
            'rules' => 'required'
        )
    ),
    'login/register' => array(
        array(
            'field' => 'first_name', 
            'label' => 'First Name', 
            'rules' => 'required|trim|htmlspecialchars|strtolower'
        ),
        array(
            'field' => 'surname', 
            'label' => 'Surname', 
            'rules' => 'trim|htmlspecialchars|strtolower'
        ),
        array(
            'field' => 'nickname', 
            'label' => 'Nickname', 
            'rules' => 'trim|htmlspecialchars|strtolower'
        ),
        array(
            'field' => 'reg_email', 
            'label' => 'Email Address', 
            'rules' => 'required|valid_email|is_unique[user.email]|trim|htmlspecialchars'
        ),
        array(
            'field' => 'reg_password', 
            'label' => 'Password', 
            'rules' => 'required'
        ),
        array(
            'field' => 'reg_confirm_password', 
            'label' => 'Confirm Password', 
            'rules' => 'required|matches[reg_password]'
        )
    )
);

