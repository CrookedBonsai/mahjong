###################
What is Mahjong?
###################

Mahjong is a complex and fun Chinese game, consisting of four players, playing elaborate tiles in a manor similar to Gin Rummy, matching pairs, three of a kind, four of a kind and many other combinations. This is very different to the Solitaire Mahjong matching games found on computers. This would be akin to comparing the Windows Solitaire card game to Poker.

****************************
Why create this application?
****************************

If you have ever played Mahjong properly, you will quickly learn that the scoring system is even more complex than the already baffling game rules.

I am creating this application to simplify the scoring procedure, make this game more accessible to players who have not yet entirely grasped the many complex and obscure rules and keep the focus on gameplay rather than calculating scores.

This application also addresses the problem of keeping a scoring tally over the course of 16+ rounds, which may be played over a succession of days, or weeks. Many games can be saved and played at the same time, keeping a running total and allowing users and spectators, to quickly glance in and see the current winner and game progression.


**********************
Current Project Status
**********************

This is a fledgling project and only the very basic structure has been set up so far. I am constantly working on this, however, so it will be fleshing out as i go along.


*******************
Server Requirements
*******************

PHP version 7.1 and Codeigniter 3.1.3 is used in the making of this project.