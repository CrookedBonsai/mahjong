<?php

/* 
 * Unit Testing
 */
class UtExampleTest extends TestCase
{
    public function __construct()
    {
        $this->controller = new UtExample();
    }
    
    public function test_return_one()
    {
        $a = $this->controller->case1();
        $this->assertEquals($a, 1);
    }
    
    public function test_return_passed_value_ten()
    {
        $var = 10;
        $a = $this->controller->case2($var);
        $this->assertEquals($a, 10);
        return $var;
    }
    
    /**
     * @depends test_return_passed_value_ten
     */
    public function test_dependant_value_from_last_function_doubled(int $var)
    {
        $var = $var*2;
        $this->assertEquals(20,$var);
    }
    
    public function test_connection_to_model()
    {
        $var = $this->controller->case3();
        echo ($a ? "True" : "False");
        $this->assertTrue($var);
    }
        
}

