<?php

/* 
 * Test Model for testing out Unit Testing Configuration
 */

class Ut_example_model_test extends TestCase
{
    public function __construct()
    {
//        parent::__construct;
        $this->model = new Ut_example_model();
         $this->example_array = array(
            "Item6"  =>  6,
            "Item7"  =>  7,
            "Item8"  =>  8,
            "Item9"  =>  9,
            "Item10" => 10
        );
        
    }
    
    
    /**
     * Return the example array
     * @return array
     */
    public function test_check_if_3rd_item_in_array_is_three()
    {
        $array = $this->model->case1();
        $a = ($array['Item3'] ? $array['Item3'] : null);
        $this->assertContains(3,$array,"Three is not found in array");
        $this->assertEquals($a,3);
    }
    
       
    /**
     * Recieve an array and add a zero to each value
     * @param array $this->example_array
     * @return array
     */
    public function test_check_passed_array()
    {
        $processed_array = $this->model->case2($this->example_array);
        $this->assertArrayHasKey("Item8", $processed_array);
        foreach ($processed_array as $key => $value)
        {
            $this->assertEquals($value, ($this->example_array[$key] * 10), "Initial value for $key -> {$this->example_array[$key]} is now: {$value} instead of: ". ($this->example_array[$key] * 10));
        }
        return $this->example_array;
    }
    
    /**
     * Confirming function without preceding test_ will not be called by Unti Test
     * @return string
     */
    public function case3()
    {
        echo "Will not be run in Unit test as it does not have a preceeding 'test_'";
        return "Fail";
    }
}

